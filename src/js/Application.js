import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    this._loading = document.querySelector(".progress");

    this._load();
    this.emit(Application.events.READY);
  }

  _create({ name, terrain, population }) {
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      name,
      terrain,
      population,
    });

    document.body.querySelector(".main").appendChild(box);
  }

  async _load(next) {
    this._startLoading();

    const data = await fetch(next || "https://swapi.boom.dev/api/planets");
    const json = await data.json();

    json.results.forEach((planet) => {
      const { name, terrain, population } = planet;
      this._create({ name, terrain, population });
    });

    this._stopLoading();
    if (json.next) await this._load(json.next);
  }

  _startLoading() {
    this._loading.style.display = "block";
  }

  _stopLoading() {
    this._loading.style.display = "none";
  }

  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
